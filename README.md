# React.js Context API: React.js + TypeScript

Example React app showcasing the concept of the React Context. The latter provides a way to pass data 
through the component tree without having to pass properties down manually at every level.
In this example, the UserContext is created and passed forward using UserProvider in `index.tsx`.

Created using:

- React.js
- TypeScript

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Layout

### Standard size
![WebsiteLayout-1](./react-ts-context-api-layout.png)
