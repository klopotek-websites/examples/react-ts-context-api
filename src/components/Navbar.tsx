import React from 'react'

import { IUser } from 'customTypes';

const Navbar: React.FC<IUser> = ({ user }): JSX.Element => {

    return (
        <div className="navbar">
            <h2 > BLOGERIZE </h2>
            <div >
                Welcome {user.name} !
            </div>
        </div>
    )
}

export default Navbar
