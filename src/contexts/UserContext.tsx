import React, { createContext, useState, FC, useContext } from "react";
import { TUser } from "customTypes";

export const defaultUser = {
    name: "",
    email: ""
};
const UserContext = createContext<TUser>(defaultUser);

export const UserProvider: FC<{ children: React.ReactNode }> = ({ children }) => {

    // Get information about the current user, here hardcoded
    const [user] = useState({ name: 'John', email: 'john@example.com' });

    return <UserContext.Provider value={user}>{children}</UserContext.Provider>
}

export const useUser = () => useContext(UserContext);
