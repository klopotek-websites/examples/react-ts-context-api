import './App.css';
import { useUser } from './contexts/UserContext';
import Navbar from './components/Navbar';

function App() {
  const user = useUser();

  return (
    <div className="App">
      <header>
        <Navbar user={user} />
      </header>
      <main>
        <div className="post-new">
          <h1> New Post</h1>

          <p>
            Lorem ipsum dolor sit amet. Qui quia beatae et voluptatum aliquam aut dolorum
            ipsam non itaque iure a voluptatem officia. Eos doloremque itaque sed quod molestias
            aut labore repellat ex voluptatum minima ut aperiam dignissimos aut tenetur pariatur
            ad voluptates voluptatum! Ea aliquam consequatur aut quasi internos eos magnam facere est
            tempore veritatis. Aut assumenda ipsa ut enim vero sed minus fugiat et beatae omnis id earum
            rerum et quia fugit ut quia galisum.
          </p>

          <p> Writen by {user.name}</p>
        </div>
      </main>
    </div>
  );
}

export default App;
