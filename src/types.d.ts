
/**
 * User-related type and interface definitions
 */
declare module "customTypes" {

    type TUser = {
        name: string,
        email: string
    };

    interface IUser {
        user: TUser;
    }

}

module.exports = {
    IUser,
    TUser,
};
